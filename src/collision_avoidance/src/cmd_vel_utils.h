#include <iostream>
using namespace std;

struct Cmd_vel{
  float x, y, z;
  
  Cmd_vel(){
    cerr << "Cmd_vel [" << this << "] default ctor" << endl;
    this->x=0.0f; 
    this->y=0.0f;
    this->z=0.0f;
  }

  Cmd_vel(float x) {
    this->x=x; 
    this->y=0.0f;
    this->z=0.0f;
  }
  
  Cmd_vel(float x, float y) {
    this->x=x; 
    this->y=y;
    this->z=0.0f;
  }

  Cmd_vel(float x, float y, float z) {
    this->x=x; 
    this->y=y;
    this->z=z;
  }

  Cmd_vel(const Cmd_vel& other){
    cerr << "Cmd_vel [" << this << "] copy ctor" << endl;
    x=other.x;
    y=other.y;
    z=other.z;
  }

  ~Cmd_vel() {
    cerr << "Cmd_vel [" << this << "] dtor" << endl;
  }
  
  
  void print() {
    cerr << "Cmd_vel::print() { x=" << x << "; y=" << y << "; z=" << z << "}" << endl;
  }

  float get_x() {return x;}

  float get_y() {return y;}

  float get_z() {return z;}

  void setValues(float x, float y, float z){
    this->x=x; 
    this->y=y;
    this->z=z;
  }
};
