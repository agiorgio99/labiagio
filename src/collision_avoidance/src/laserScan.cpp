#include "ros/ros.h"

#include "sensor_msgs/LaserScan.h" //per leggere i messaggi del laserScanner
#include "tf/transform_listener.h" //per il listener della trasformazione
#include "geometry_utils_fd.h" //per il metodo convertPose2D
#include "cmd_vel_utils.h" //per la struct del cmd_vel

Cmd_vel cmd_vel;
ros::Publisher cmd_vel_pub;

void save_initial_cmd_vel(const geometry_msgs::Twist::ConstPtr& msg){
  //uso metodo ausiliario della classe Cmd_vel, dichiarata nel cmd_vel_utils.h, per settare i valori del comando di velocità iniziale
  cmd_vel.setValues(msg->linear.x, msg->linear.y, msg->angular.z);
}

void laserScanCallback(const sensor_msgs::LaserScan::ConstPtr& scan){
  //se nessun comando è stato impartito, allora la callback viene terminata e il robot non si muove
  //per semplicità faccio andare a destra/sinistra usando direttamente la componente di rotazione z, perciò y è sempre = 0
  if(cmd_vel.x == 0.0f && cmd_vel.z == 0.0f) return; 

  float forza_ris_x=0.0f;
  float forza_ris_y=0.0f;

  tf::StampedTransform transform;
  tf::TransformListener listener;

  try{
    //blocca fino a quando la trasformazione dal frame della stanza a quello dello scanner non è disponibile 
    //o fino a quando il timeout non è scaduto (in questo caso esce)
    if(!listener.waitForTransform( 
              "base_link",
              "base_laser_link",
              ros::Time(0),
              ros::Duration(10.0))){
      return;
    }
    //trasforma le coordinate dal frame del laserScan a quelle del frame del robot
    listener.lookupTransform("base_link", "base_laser_link", ros::Time(0), transform); 
  }
  catch (tf::TransformException ex) {
    //stampa eccezione in caso di errore e si mette a dormire
    ROS_ERROR("%s",ex.what());
    ros::Duration(1.0).sleep();
  }
  
  //prende la trasformata e produce un'isometria da essa
  Eigen::Isometry2f isometria = convertPose2D(transform); 
  
  Eigen::Vector2f punto;
  float theta;
  int i, count=0;
  for(i=0; i<scan->ranges.size(); i++){ //per ogni punto scannerizzato
    //valore punto i-esimo
    float p= scan->ranges[i];
    //angolo corrispondente al punto i-esimo
    theta= scan->angle_min+i*scan->angle_increment;
    //coordinate (x,y) equivalenti del punto i-esimo
    punto(0)= p*std::cos(theta);
    punto(1)= p*std::sin(theta);
    //trasformo il punto dell'ostacolo i-esimo per averlo nel sistema di riferimento del robot
    punto= isometria * punto;
    //calcolo norma vettore differenza tra t(x,y)=posizione del robot=(0,0) (dato che stiamo considerando come sistema di riferimento quello del robot) 
    //e p_i(x,y)=(punto(0), punto(1)), ovvero t-p_i=(-punto(0), -punto(1))
    float norma= sqrt(pow(-punto(0), 2) + pow(-punto(1), 2)); //non con sqrt per ridurre modulo forza risultante all'aumentare della distanza dall'ostacolo, infatti al denominatore è come se avessimo norma (distanza) al quadrato
    //printf("\nla norma per il punto %d-esimo è %f\n", i, norma);
    //calcolo forza risultante in modo tale da poter deviare la traiettoria del robot per evitare l'ostacolo
    float modulo_forza_x= punto(0)/pow(norma, 2); //modulo forza risultante x= x/norma
    float modulo_forza_y= punto(1)/pow(norma, 2); //modulo forza risultante y= y/norma
    //sommo le componenti trovate a quelle totali
    if(norma<3.0){
      count++;
      forza_ris_x+= 3*modulo_forza_x;
      forza_ris_y+= modulo_forza_y;
    }
    else{
      forza_ris_x+= modulo_forza_x;
      forza_ris_y+= modulo_forza_y;
    }
  }
  //dato che la forza risultante è in verso opposto rispetto a quello del vettore spostamento del robot, allora cambio segno alle componenti risultanti
  forza_ris_x= -forza_ris_x;
  forza_ris_y= -forza_ris_y;
  float percentuale= (float)(count*100)/(float)scan->ranges.size();
  printf("forza_ris_x=%f, forza_ris_y=%f, punti con norma<3=%d, percentuale di punti con norma<3=%f\n", forza_ris_x, forza_ris_y, count, percentuale);
  //setto nuovo comando di velocità opportunamente calibrato con dei coefficienti moltiplicativi correttivi per le forze risultanti
  float coeff_x= cmd_vel.x >= 10.0 ? 0.0025 : 0.002;
  float vel_x= coeff_x*forza_ris_x + cmd_vel.x;
  float vel_z= 0.00167*forza_ris_y + cmd_vel.z;

  cmd_vel.setValues(vel_x, 0.0f, vel_z);
  cmd_vel.print();
  geometry_msgs::Twist velocity_msg;
  velocity_msg.linear.x=cmd_vel.x;
  velocity_msg.linear.y=cmd_vel.y;
  velocity_msg.angular.z=cmd_vel.z;
  //pubblico comando definitivo sul topic /cmd_vel
  cmd_vel_pub.publish(velocity_msg);
  cmd_vel.setValues(0.0f, 0.0f, 0.0f);
}


int main(int argc, char **argv){

  ros::init(argc, argv, "laserScan");

  ros::NodeHandle n;
  //mi sottoscrivo al topic ausiliario da dove si riceve il comando di velocità iniziale, con callBack che salva i valori nella variabile globale cmd_vel
  ros::Subscriber sub = n.subscribe("/receiver", 1000, save_initial_cmd_vel);
  //mi sottoscrivo al topic del laserScan dal quale ottengo i punti scannerizzati degli ostacoli, recepiti dalla callBack "laserScanCallBack"
  ros::Subscriber laserScansub = n.subscribe("/base_scan", 1000, laserScanCallback);
  //mi iscrivo al topic /cmd_vel come publisher
  cmd_vel_pub = n.advertise<geometry_msgs::Twist>("/cmd_vel", 1000);
  //faccio eseguire le callBacks pendenti
  ros::spin();

  return 0;
}
