# CMake generated Testfile for 
# Source directory: /home/agiorgio99/catkin_ws/src
# Build directory: /home/agiorgio99/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("beginner_tutorials")
subdirs("collision_avoidance")
