PROGETTO LABIAGI A.A. 2020/2021

STUDENTE: Antonello Giorgio 1836529 giorgio.1836529@studenti.uniroma1.it

TITOLO: Collision Avoidance

ISTRUZIONI PER TESTING:

Aprire tre terminali, in cui (TX= terminale X):

T1:
Eseguire comandi

- "git clone https://gitlab.com/grisetti/labiagi_2020_21" per scaricare materiale necessario per mostrare la mappa con il robot

- "cd ~/<path della directory in cui sta la cartella scaricata>/labiagi_2020_21/workspaces/srrg2_labiagi/src/srrg2_navigation_2d/config" 

- source ~/<path della directory in cui sta la cartella scaricata>/labiagi_2020_21/workspaces/srrg2_labiagi/devel/setup.bash

- ~/<path della directory in cui sta la cartella scaricata>/labiagi_2020_21/srrg2_webctl/proc_webctl run_navigation.webctl

Infine aprire browser qualsiasi, connettersi alla porta 9001 digitando link "localhost:9001" e premere uno alla volta il pulsante "start" dei primi cinque comandi.

T2:

Eseguire comandi

- "git clone https://gitlab.com/agiorgio99/labiagio"

- "cd ~/<path della directory in cui sta la cartella scaricata>/labiagio"

- "catkin_make"

- "source devel/setup.bash"

- "rosrun collision_avoidance laserScan"

T3:

Eseguire comando ("c"=cifra qualsiasi) rostopic pub /receiver geometry_msgs/Twist -r 1 "linear: 

  x: c.c
  y: 0.0
  z: 0.0

angular:
  x: 0.0
  y: 0.0
  z: c.c"








